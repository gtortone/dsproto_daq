#!/usr/bin/python3

import os
import re
import sys

arg = str(sys.argv[1])

if(not os.path.isfile(arg)):
    print("E: provided argument is not a file")
    sys.exit(0)

basepath = "/storage/dsdata/MIDAS/"
fname = os.path.basename(arg)
#print(basepath)
#print(fname)

x = re.findall('\d+', fname)
run = x[0]

rundir = basepath + "/run" + run + "/"
#print(rundir)
try:
    os.mkdir(rundir)
except FileExistsError:
    pass

# check if destination file exists
# ...

cmd = "/opt/dsproto_analyzer/runconv 1>/dev/null 2>/dev/null " + arg
os.system(cmd)

os.rename(arg, rundir + fname)

