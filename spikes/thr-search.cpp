#include <stdio.h>
#include <string>
#include <sys/time.h>
#include <CAENComm.h>
#include <CAENVMElib.h>
#include "v1725Raw.h"

#define QUIET true

// dump event on screen
//#define DUMP_EVENT

CAENComm_ErrorCode WriteReg(int devh, uint32_t addr, uint32_t value, std::string regdesc, bool quiet=false);
CAENComm_ErrorCode ReadReg(int devh, uint32_t addr, uint32_t &value, std::string regdesc, bool quiet=false);

int main(int argc, char **argv) {

   int c;
   int nconv;
   int link = 0; 
   int board = 0;
   int channel = 0;

   int devh;
   uint32_t readval;
   CAENComm_ErrorCode retval;

   int nw;
   uint32_t *pdata, data[50000];
   uint32_t eSize;
   uint32_t threshold;
   bool finished = false;

   // parse command line arguments
   while ((c = getopt (argc, argv, "l:b:c:")) != -1)
      switch (c) {
  
         case 'l':
            nconv = sscanf(optarg, "%d", &link);
            if( (nconv != 1) || (link>4) || (link<0) ){
               printf("E: link number not valid\n");
	       exit(1);
            } 
         break;

         case 'b':
            nconv = sscanf(optarg, "%d", &board);
            if( (nconv != 1) || (board>32) || (board<0) ){
               printf("E: board number not valid\n");
	       exit(1);
            } 
         break;

            
         case 'c':
            nconv = sscanf(optarg, "%d", &channel);
            if( (nconv != 1) || (channel>15) || (channel<0) ){
               printf("E: channel number not valid\n");
	       exit(1);
            } 
         break;

         default:
         break;
      }

   printf(">>>> LINK: %d   -   BOARD: %d    -    CHANNEL: %d <<<<\n", link, board, channel);

   printf("I: OpenDevice\n");
   retval = CAENComm_OpenDevice(CAENComm_PCIE_OpticalLink, link, board, 0, &devh);

   if(retval != CAENComm_Success) {
      printf("E: OpenDevice\n");
      exit(1);
   }

   printf("(RESET)\n");
   WriteReg(devh, V1725_SW_RESET,			0x0001, "V1725_SW_RESET");
   sleep(1);

   printf("(INFO)\n");
   ReadReg(devh, V1725_ROC_FPGA_FW_REV, readval, "V1725_ROC_FPGA_FW_REV");
   ReadReg(devh, V1725_FPGA_FWREV, readval, "V1725_FPGA_FWREV");
   ReadReg(devh, V1725_VME_STATUS, readval, "V1725_VME_STATUS");
   ReadReg(devh, V1725_CHANNEL_STATUS + (channel*0x0100), readval, "V1725_CHANNEL_STATUS");
   ReadReg(devh, V1725_CHANNEL_DAC + (channel*0x0100), readval, "V1725_CHANNEL_DAC");
   ReadReg(devh, 0x1084 + (channel*0x0100), readval, "V1725_SELFTRIGGER_LOGIC");
   ReadReg(devh, 0x1070 + (channel*0x0100), readval, "V1725_PULSE_WIDTH");

   printf("(SETUP)\n");
   WriteReg(devh, V1725_BOARD_CONFIG, 			0x0000, "V1725_BOARD_CONFIG");
   WriteReg(devh, V1725_ACQUISITION_CONTROL, 		0x0000, "V1725_ACQUISITION_CONTROL");
   WriteReg(devh, V1725_BUFFER_ORGANIZATION, 		0x000A, "V1725_BUFFER_ORGANIZATION");
   WriteReg(devh, V1725_CHANNEL_EN_MASK, 		(channel==0)?1:1<<(channel), "V1725_CHANNEL_EN_MASK");
   WriteReg(devh, V1725_TRIG_SRCE_EN_MASK, 		(channel==0)?1:1<<(channel/2), "V1725_TRIG_SRCE_EN_MASK");
   WriteReg(devh, 0x1084 + (channel*0x0100), 		0x0001, "V1725_SELFTRIGGER_LOGIC");
   WriteReg(devh, V1725_BLT_EVENT_NB,			0x0001, "V1725_BLT_EVENT_NB");
   
   printf("\npress ENTER to threshold search on CHANNEL %d...", channel);
   getchar();

   printf("(START)\n");
   WriteReg(devh, V1725_ACQUISITION_CONTROL, 0x0004, "V1725_ACQUISITION_CONTROL"); 
   sleep(1);

   printf("(ACQUIRE)\n"); 

   threshold = 0;
   //threshold = 0x3FFF;		// max value
   while( (threshold <= 0x3FFF) && !finished ) {
   //while( (threshold > 0) && !finished ) {

      WriteReg(devh, V1725_ACQUISITION_CONTROL, 0x0000, "V1725_ACQUISITION_CONTROL", QUIET); 
      WriteReg(devh, V1725_CHANNEL_THRESHOLD + (channel*0x0100), threshold, "V1725_CHANNEL_THRESHOLD", QUIET);
      WriteReg(devh, V1725_ACQUISITION_CONTROL, 0x0004, "V1725_ACQUISITION_CONTROL", QUIET); 
      usleep(2000);

      printf("\r L:%d B:%d CH:%d / threshold value: 0x%04X", link, board, channel, threshold);

/*
      ReadReg(devh, V1725_VME_STATUS, readval, "V1725_VME_STATUS");
      ReadReg(devh, V1725_ACQUISITION_STATUS, readval, "V1725_ACQUISITION_STATUS");
      ReadReg(devh, V1725_CHANNEL_STATUS, readval, "V1725_CHANNEL_STATUS");
      ReadReg(devh, V1725_CHANNEL_STATUS+0x0100, readval, "V1725_CHANNEL_STATUS");
      ReadReg(devh, V1725_EVENT_STORED, readval, "V1725_EVENT_STORED");
*/
      ReadReg(devh, V1725_EVENT_SIZE, eSize, "V1725_EVENT_SIZE", QUIET);

      pdata = &data[0];
      nw = 0;

      uint32_t size = eSize;
      do {
         CAENComm_BLTRead(devh, V1725_EVENT_READOUT_BUFFER, pdata, eSize < 37500 ? eSize : 37500, &nw);		// was 1028
         eSize -= nw;
         pdata += nw;
      } while (eSize);

      if(size) {
         //finished = true;
         printf("\n\n>>> threshold for link %d / channel %d : 0x%04X (%d) <<<\n", link, channel, threshold, threshold);
#ifdef DUMP_EVENT
         for(unsigned int i=0; i<size; i++) {
            uint16_t w0 = data[i] & 0x0000FFFF;
            uint16_t w1 = data[i] & 0xFFFF0000 >> 16;
            printf("0x%04X . 0x%04X", w0, w1);
            (i%8 == 0)?printf("\n"):printf(" . ");
         }
#endif
      }

      threshold++;
      //threshold--;

   }	// end while


   if(finished)
      printf("\n\n>>> threshold for link %d / channel %d : 0x%04X (%d) <<<\n", link, channel, threshold, threshold);

   return 0;
}

CAENComm_ErrorCode WriteReg(int devh, uint32_t addr, uint32_t value, std::string regdesc, bool quiet) {

   CAENComm_ErrorCode retval;

   if(!quiet) printf("I: WRITE addr 0x%04X (%s), val 0x%04X\n", addr, regdesc.c_str(), value);
   retval = CAENComm_Write32(devh, addr, value);
   if(retval != CAENComm_Success) {
      printf("E: Write %s\n", regdesc.c_str());
      exit(1);
   }
  
   return retval;
}

CAENComm_ErrorCode ReadReg(int devh, uint32_t addr, uint32_t &value, std::string regdesc, bool quiet) {

   CAENComm_ErrorCode retval;

   retval = CAENComm_Read32(devh, addr, &value);
   if(retval != CAENComm_Success) {
      printf("E: Read %s\n", regdesc.c_str());
      exit(1);
   } else {
      if(!quiet) printf("I: READ addr 0x%04X (%s), val 0x%04X\n", addr, regdesc.c_str(), value);
   }

   return retval;
}
