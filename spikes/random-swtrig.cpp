#include <stdio.h>
#include <string>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <CAENComm.h>
#include <CAENVMElib.h>
#include "v1725Raw.h"

#define QUIET true

// dump event on screen
//#define DUMP_EVENT

CAENComm_ErrorCode WriteReg(int devh, uint32_t addr, uint32_t value, std::string regdesc, bool quiet=false);
CAENComm_ErrorCode ReadReg(int devh, uint32_t addr, uint32_t &value, std::string regdesc, bool quiet=false);

int main(int argc, char **argv) {

   int c;
   int nconv;
   int link = 0; 
   int board = 0;

   int devh;
   CAENComm_ErrorCode retval;

   // parse command line arguments
   while ((c = getopt (argc, argv, "l:b:c:")) != -1)
      switch (c) {
  
         case 'l':
            nconv = sscanf(optarg, "%d", &link);
            if( (nconv != 1) || (link>4) || (link<0) ){
               printf("E: link number not valid\n");
	       exit(1);
            } 
         break;

         case 'b':
            nconv = sscanf(optarg, "%d", &board);
            if( (nconv != 1) || (board>32) || (board<0) ){
               printf("E: board number not valid\n");
	       exit(1);
            } 
         break;

         default:
         break;
      }

   printf(">>>> LINK: %d   -   BOARD: %d <<<<\n", link, board);

   printf("I: OpenDevice\n");
   retval = CAENComm_OpenDevice(CAENComm_PCIE_OpticalLink, link, board, 0, &devh);

   if(retval != CAENComm_Success) {
      printf("E: OpenDevice\n");
      exit(1);
   }

   srand(time(NULL)); 
   while(true) {
      
      float r = (double)(rand()) / RAND_MAX;

      if (r >= 0.5)
         WriteReg(devh, V1725_SW_TRIGGER, 0x0001, "V1725_SW_TRIGGER", QUIET); 

      usleep(200 * 1000);   // sleep 200 msec
   }

   CAENComm_CloseDevice(devh);
}

CAENComm_ErrorCode WriteReg(int devh, uint32_t addr, uint32_t value, std::string regdesc, bool quiet) {

   CAENComm_ErrorCode retval;

   if(!quiet) printf("I: WRITE addr 0x%04X (%s), val 0x%04X\n", addr, regdesc.c_str(), value);
   retval = CAENComm_Write32(devh, addr, value);
   if(retval != CAENComm_Success) {
      printf("E: Write %s\n", regdesc.c_str());
      exit(1);
   }
  
   return retval;
}

CAENComm_ErrorCode ReadReg(int devh, uint32_t addr, uint32_t &value, std::string regdesc, bool quiet) {

   CAENComm_ErrorCode retval;

   retval = CAENComm_Read32(devh, addr, &value);
   if(retval != CAENComm_Success) {
      printf("E: Read %s\n", regdesc.c_str());
      exit(1);
   } else {
      if(!quiet) printf("I: READ addr 0x%04X (%s), val 0x%04X\n", addr, regdesc.c_str(), value);
   }

   return retval;
}
