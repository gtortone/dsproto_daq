#include <stdio.h>
#include <string>
#include <sys/time.h>
#include <CAENComm.h>
#include <CAENVMElib.h>
#include "v1725Raw.h"

#define QUIET true

// dump event on screen
#define DUMP_EVENT

// triangle wave test signal
//#define WAVE_TRI

// show bandwith in MB/s
#define SHOW_BW	

// use software trigger
//#define TRIG_SW

#define OPTICAL_LINK	0

CAENComm_ErrorCode WriteReg(int devh, uint32_t addr, uint32_t value, std::string regdesc, bool quiet=false);
CAENComm_ErrorCode ReadReg(int devh, uint32_t addr, uint32_t &value, std::string regdesc, bool quiet=false);

int main(void) {

   int devh;
   uint32_t readval;
   CAENComm_ErrorCode retval;

   int nw;
   uint32_t *pdata, data[50000];
   uint32_t eSize;
   long int totw, sampledw;
#ifdef SHOW_BW
   struct timeval start, end, last;
#endif 

   printf("I: OpenDevice\n");
   retval = CAENComm_OpenDevice(CAENComm_PCIE_OpticalLink, OPTICAL_LINK, 0, 0, &devh);

   if(retval != CAENComm_Success) {
      printf("E: OpenDevice\n");
      exit(1);
   }

   printf("(RESET)\n");
   WriteReg(devh, V1725_CONFIG_RELOAD,			0x0001, "V1725_CONFIG_RELOAD");
   WriteReg(devh, V1725_SW_RESET,			0x0001, "V1725_SW_RESET");
   WriteReg(devh, V1725_SW_CLEAR,			0x0001, "V1725_SW_CLEAR");
   sleep(1);

   printf("(INFO)\n");
   ReadReg(devh, V1725_ROC_FPGA_FW_REV, readval, "V1725_ROC_FPGA_FW_REV");
   ReadReg(devh, V1725_FPGA_FWREV, readval, "V1725_FPGA_FWREV");
   ReadReg(devh, V1725_VME_STATUS, readval, "V1725_VME_STATUS");
   ReadReg(devh, V1725_ZS_NSAMP+0x0200, readval, "V1725_ZS_NSAMP[2]");
   ReadReg(devh, V1725_CHANNEL_DAC+0x0200, readval, "V1725_CHANNEL_DAC[2]");
   ReadReg(devh, V1725_CHANNEL_STATUS+0x0200, readval, "V1725_CHANNEL_STATUS[2]");
   ReadReg(devh, V1725_POST_TRIGGER_SETTING, readval, "V1725_POST_TRIGGER_SETTING");
   ReadReg(devh, V1725_FP_IO_CONTROL, readval, "V1725_FP_IO_CONTROL");

   printf("(SETUP)\n");
   WriteReg(devh, V1725_ZS_NSAMP+0x0200, 		0x0000, "V1725_ZS_NSAMP[2]");		// set input dynamic range of CH2 to 2Vpp
#ifdef WAVE_TRI
   WriteReg(devh, V1725_BOARD_CONFIG, 			0x0008, "V1725_BOARD_CONFIG");		// triangular (0-3FFF) test wave
#else
   WriteReg(devh, V1725_BOARD_CONFIG, 			0x0000, "V1725_BOARD_CONFIG");
#endif
   WriteReg(devh, V1725_ACQUISITION_CONTROL, 		0x0000, "V1725_ACQUISITION_CONTROL");
   WriteReg(devh, V1725_BUFFER_ORGANIZATION, 		0x000A, "V1725_BUFFER_ORGANIZATION");
   WriteReg(devh, V1725_CHANNEL_EN_MASK, 		0x0004, "V1725_CHANNEL_EN_MASK");	// enable ch02 only

   //
   WriteReg(devh, V1725_CHANNEL_DAC+0x0200,		0x8000, "V1725_CHANNEL_DAC[2]");	// set ch02 DC offset to FSR/2
   printf("Apply DAC offset ....");
   do {

      ReadReg(devh, V1725_CHANNEL_STATUS+0x0200, readval, "V1725_CHANNEL_STATUS[2]", QUIET);

   } while ( (readval & 0x0004) != 0);

   printf(" DONE !\n");

   //
   WriteReg(devh, V1725_ADC_CALIBRATION,                0x0001, "V1725_ADC_CALIBRATION");
   printf("Start calibration....");

   do {

      ReadReg(devh, V1725_CHANNEL_STATUS+0x0200, readval, "V1725_CHANNEL_STATUS[2]", QUIET);

   } while ( (readval & 0x0008) == 0);

   printf(" DONE !\n");

   WriteReg(devh, V1725_COUPLE_TRIGGER_LOGIC+0x0200,	0x0003, "V1725_COUPLE_TRIGGER_LOGIC[CH2-CH3]");// set trigger logic of couple CH2-CH3 to pulse (reg 0x8070 / 0x1n70 - default 2 -> 32 ns)
   //WriteReg(devh, V1725_PULSE_WIDTH+0x0200,		0x0001, "V1725_PULSE_WIDTH[2]");	// set trigger pulse to 16 ns (16ns * 1)
   WriteReg(devh, V1725_CHANNEL_THRESHOLD+0x0200,	0x1F40, "V1725_CHANNEL_THRESHOLD[2]");	// set threshold of ch02
   WriteReg(devh, V1725_TRIG_SRCE_EN_MASK, 		0x0002, "V1725_TRIG_SRCE_EN_MASK");	// enable couples 2,3

   WriteReg(devh, V1725_BLT_EVENT_NB,			0x0001, "V1725_BLT_EVENT_NB");
   
   printf("\npress ENTER to start acquisition...");
   getchar();

   printf("(START)\n");
   WriteReg(devh, V1725_ACQUISITION_CONTROL, 		0x0004, "V1725_ACQUISITION_CONTROL"); 
   sleep(1);

   printf("(ACQUIRE)\n"); 

   totw = 0;
   sampledw = 0;
#ifdef SHOW_BW
   gettimeofday(&start, NULL);
   gettimeofday(&last, NULL);
#endif
   while(true) {

#ifdef TRIG_SW
      WriteReg(devh, V1725_SW_TRIGGER, 0x0001, "V1725_SW_TRIGGER");
#endif

#ifndef SHOW_BW
      ReadReg(devh, V1725_VME_STATUS, readval, "V1725_VME_STATUS");
      ReadReg(devh, V1725_ACQUISITION_STATUS, readval, "V1725_ACQUISITION_STATUS");
      ReadReg(devh, V1725_CHANNEL_STATUS+0x0200, readval, "V1725_CHANNEL_STATUS[2]");
      ReadReg(devh, V1725_EVENT_STORED, readval, "V1725_EVENT_STORED");
      ReadReg(devh, V1725_EVENT_SIZE, eSize, "V1725_EVENT_SIZE");
#else
      ReadReg(devh, V1725_EVENT_SIZE, eSize, "V1725_EVENT_SIZE", QUIET);
#endif

      pdata = &data[0];
      nw = 0;

#ifdef DUMP_EVENT
      uint32_t size = eSize;
#endif
      do {
         CAENComm_BLTRead(devh, V1725_EVENT_READOUT_BUFFER, pdata, eSize < 1028 ? eSize : 1028 , &nw);
         eSize -= nw;
         pdata += nw;
         totw += nw;
         sampledw += nw;
      } while (eSize);

#ifdef DUMP_EVENT
      if(size) {

         printf("=== EVENT START (size = %lu - samples = %lu) ===\n\n", (unsigned long)size, (unsigned long)size*2);

         for(unsigned int i=0; i<size; i++) {
            printf(" %05d ", (data[i] & 0x0000FFFF));
            printf(" %05d ", (data[i] & 0xFFFF0000) >> 16);
            (i%8 == 0)?printf("\n"):0;
         }

         printf("=== EVENT END ===\n\n");

      }
#endif

#ifdef SHOW_BW
      gettimeofday(&end, NULL);
      
      if( (end.tv_sec - last.tv_sec) >= 10) {

         float bw = (sampledw*4) / 1000000.0 / ((end.tv_sec*1000000 + end.tv_usec - last.tv_sec*1000000 + last.tv_usec)/1000000.0);
         printf("Bandwith: %02f MB/s\n", bw);
         gettimeofday(&last, NULL);
         sampledw = 0;
      }
#endif
#ifdef TRIG_SW
      printf("Press enter to send a software trigger...");
      getchar();
#endif
   }

   return 0;
}

CAENComm_ErrorCode WriteReg(int devh, uint32_t addr, uint32_t value, std::string regdesc, bool quiet) {

   CAENComm_ErrorCode retval;

   if(!quiet) printf("I: WRITE addr 0x%04X (%s), val 0x%04X\n", addr, regdesc.c_str(), value);
   retval = CAENComm_Write32(devh, addr, value);
   if(retval != CAENComm_Success) {
      printf("E: Write %s\n", regdesc.c_str());
      exit(1);
   }
  
   return retval;
}

CAENComm_ErrorCode ReadReg(int devh, uint32_t addr, uint32_t &value, std::string regdesc, bool quiet) {

   CAENComm_ErrorCode retval;

   retval = CAENComm_Read32(devh, addr, &value);
   if(retval != CAENComm_Success) {
      printf("E: Read %s\n", regdesc.c_str());
      exit(1);
   } else {
      if(!quiet) printf("I: READ addr 0x%04X (%s), val 0x%04X\n", addr, regdesc.c_str(), value);
   }

   return retval;
}
