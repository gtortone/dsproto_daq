####################################################################
#
#  Name:         Makefile
#  Created by:   Thomas Lindner
#
#  Contents:     Makefile for the v1725 frontend
#
#  Copied from DEAP frontend
#
#####################################################################

# Hardware setup
USE_SYSTEM_BUFFER=1

#--------------------------------------------------------------------
# The MIDASSYS should be defined prior the use of this Makefile
ifndef MIDASSYS
missmidas::
	@echo "...";
	@echo "Missing definition of environment variable 'MIDASSYS' !";
	@echo "...";
endif

OSFLAGS = -DOS_LINUX -DLINUX
CFLAGS = -g -Wall -pthread 
#For backtrace
#CFLAGS = -g -Wall -pthread -rdynamic -fno-omit-frame-pointer -fno-inline -fno-inline-functions
LDFLAGS = -g -lm -lz -lutil -lnsl -lpthread -lrt -lc -lhiredis

CFLAGS += -O2

MIDAS_INC    = $(MIDASSYS)/include
MIDAS_LIB    = $(MIDASSYS)/lib
MIDAS_SRC    = $(MIDASSYS)/src

####################################################################
# Lines below here should not be edited
####################################################################
#
CC   = gcc # -std=c99
CXX  = g++ -std=c++11

LIBMIDAS=-L$(MIDAS_LIB) -lmidas
LIBCAENCOMM=-lCAENComm
LIBCAENVME=-lCAENVME

LIBALL= $(LIBMIDAS) $(LIBCAENCOMM) $(LIBCAENVME) $(LIBZMQ)

INCS = -I. -I$(MIDAS_INC) -I/usr/include/hiredis

####################################################################
# General commands
####################################################################

all: fe

fe : feoV1725mt.exe

doc ::
	doxygen
	@echo "***** Use firefox --no-remote doc/html/index.html to view if outside gateway"

####################################################################
# Libraries/shared stuff
####################################################################

ov1725.o : ov1725.c
	$(CC) -c $(CFLAGS) $(INCS) $< -o $@ 

####################################################################
# Multi-thread frontend
####################################################################

feoV1725mt.exe: $(MIDAS_LIB)/mfe.o  feoV1725.o ov1725.o v1725CONET2.o
	$(CXX) $(OSFLAGS) feoV1725.o v1725CONET2.o ov1725.o $(MIDAS_LIB)/mfe.o $(LIBALL) -o $@ $(LDFLAGS)

feoV1725.o : feoV1725.cxx v1725CONET2.o
	$(CXX) $(CFLAGS) $(OSFLAGS) $(INCS) -I. -Ife -c $< -o $@

v1725CONET2.o : v1725CONET2.cxx
	$(CXX) $(CFLAGS) $(OSFLAGS) $(INCS) -Ife -c $< -o $@

$(MIDAS_LIB)/mfe.o:
	@cd $(MIDASSYS) && make
####################################################################
# Clean
####################################################################

clean:
	rm -f *.o *.exe
	rm -f *~
