#ifndef _SETUP_H_
#define _SETUP_H_

#define NBLINKSPERA3818   	4   //!< Number of optical links used per A3818
#define NBLINKSPERFE      	4   //!< Number of optical links controlled by each frontend
#define NB1725PERLINK     	1   //!< Number of daisy-chained v1725s per optical link
#define NBV1725TOTAL      	4   //!< Number of v1725 boards in total
#define NBCORES           	8   //!< Number of cpu cores, for process/thread locking

/*
// test for full crate of 20 V1725B boards
#define NBLINKSPERA3818    4   //!< Number of optical links used per A3818
#define NBLINKSPERFE       1   //!< Number of optical links controlled by each frontend
#define NB1725PERLINK      5   //!< Number of daisy-chained v1725s per optical link
#define NBV1725TOTAL       20  //!< Number of v1725 boards in total
#define NBCORES            8   //!< Number of cpu cores, for process/thread locking
*/

#define USE_SYSTEM_BUFFER	1

#endif
